﻿using UnityEngine;
using System.Collections;

public class CannonController2D : MonoBehaviour
{
    public static float deg2rad = 0.01745329251f;

    [SerializeField]
    GameObject cannonBallPrefab;
    [SerializeField]
    Transform muzzlePoint;

    [SerializeField]
    RenderBallisticPath2D pathRenderer2D;

    public float initialCannonSpeed = 10f;
    public float angle = 0;

    public Transform aimTarget;


    #region Unity
    void Update()
    {
        HandleInput();
        UpdatePathRenderer();
    }
    #endregion


    void UpdatePathRenderer()
    {
        pathRenderer2D.initialSpeed = initialCannonSpeed;
        angle = pathRenderer2D.angle;
        //pathRenderer2D.aimTarget = aimTarget;
        Vector2 mousePos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
        pathRenderer2D.aimTarget.position = mousePos;
    }

    void HandleInput()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Fire();
        }
    }

    void Fire()
    {
        GameObject obj = (GameObject)GameObject.Instantiate(cannonBallPrefab, muzzlePoint.position, Quaternion.identity);

        StartCoroutine(DestroyObjTimeout(obj, 5));
        Vector2 vel = CalculateInitialVelocity();
        obj.GetComponent<Rigidbody2D>().velocity = vel;
    }

    Vector2 CalculateInitialVelocity()
    {
        // initial vel should 
        float radTheta = angle * deg2rad;
        Vector2 dir;
        dir.x = Mathf.Cos(radTheta);
        dir.y = Mathf.Sin(radTheta);

        return (dir * initialCannonSpeed);
    }

    IEnumerator DestroyObjTimeout(GameObject obj, float timeout)
    {
        yield return new WaitForSeconds(timeout);

        GameObject.Destroy(obj);
    }
}
