﻿using UnityEngine;
using System.Collections;


public class RenderBallisticPath2D : MonoBehaviour
{
    #region Member Fields
    public bool showMinArc, showMaxArc, showSolutionArc;

    [Space(15)]

    // The "source" of the path. AKA, where the path should start drawing from.
    [SerializeField]
    Transform projectileSource;

    // The pivot point of whatever the projectile is coming from. 
    // This is where the angle actually is.
    [SerializeField]
    Transform projectileSourcePivot;

    /// LineRenderer used to draw min angle path
    [SerializeField]
    LineRenderer minAngleLineRenderer;
    /// LineRenderer used to draw max angle path
    [SerializeField]
    LineRenderer maxAngleLineRenderer;
    /// LineRenderer used to draw solution path (where barrel is currently aiming)
    [SerializeField]
    LineRenderer solutionLineRenderer;

    [Space(15)]

    /// Magnitude of initial velocity
    public float initialSpeed = 10.0f;

    /// Firing angle
    //[Range(0f, 360f)]
    public float angle = 0;

    public float minAngle = 0;
    public float maxAngle = 0;

    /// Time interval to calculate (lower is smoother; should match fixedDeltaTime)
    float timeResolution = .02f;

    /// Max time to simulate path for
    [Range(0f, 10f)]
    public float maxTime = 10.0f;

    /// Mask defining what this projectil path collides with
    public LayerMask layerMask = -1;

    /// 1 degree = pi/180 rads. Used to convert angle to radians
    public static float deg2rad = 0.01745329251f;

    [Space(15)]

    // Target to aim at
    public Transform aimTarget = null;
    // Should I be keeping track ofsome target?
    [SerializeField]
    private bool testMinAndMax = false;
    // Is my target in range?
    [SerializeField]
    bool targetInRange = false;

    [Space(10)]
    // Should I take control of the barrel's aim?
    public bool controlBarrel = false;
    // How long should it take to reach my new angle?
    [SerializeField]
    float barrelLerpTime = .4f;
    // Am I currently lerping?
    bool lerpingAngle = false;

    // Used to help the maths figure out correct angles
    bool flipped = false;

    [Space(15)]

    // Flags to test whether angles are blocked, and locks 
    //  to prevent them from being changed temporarily.
    [SerializeField]
    bool minAngleBlocked = false;
    bool minBlockLock = false;
    [SerializeField]
    bool maxAngleBlocked = false;
    bool maxBlockLock = false;

    // How long until checking for minimal angle after switching to max angle?
    [SerializeField]
    float angleCheckCD = .5f;
    #endregion


    #region Unity Methods
    void Update()
    {
        if (testMinAndMax && aimTarget != null && !TargetTooClose())
        {
            minAngleLineRenderer.enabled = showMinArc;
            maxAngleLineRenderer.enabled = showMaxArc;
            UpdateWithTarget(aimTarget.position);
        }
        else
        {
            minAngleLineRenderer.enabled = false;
            maxAngleLineRenderer.enabled = false;
            flipped = false;
        }
    }

    void LateUpdate()
    {
        if (controlBarrel)
        {
            float theta = FindOptimalAngleTo(aimTarget.position);
            if (!float.IsNaN(theta))
            {
                StartCoroutine(AimBarrel(theta, barrelLerpTime));
            }
        }

        UpdatePivot();
        // render solution path
        solutionLineRenderer.enabled = showSolutionArc;
        RenderPath(solutionLineRenderer, angle);
    }
    #endregion

    /// <summary>
    /// How to handle update loop while I should be aiming at a target
    /// </summary>
    void UpdateWithTarget(Vector2 t)
    {
        float determ = TargetRangeCheck(t);

        targetInRange = (determ != -1);

        UpdateFlip(t);

        if (targetInRange)
        {
            // calculate min and max angles
            minAngle = CalculateMinAngle(determ);
            maxAngle = CalculateMaxAngle(determ);

            if (!minBlockLock)
            {
                minAngleBlocked = RenderPath(minAngleLineRenderer, minAngle, true);
            }
            if (!maxBlockLock)
            {
                maxAngleBlocked = RenderPath(maxAngleLineRenderer, maxAngle, false);
            }
        }
        else
        {
            minAngleLineRenderer.enabled = false;
            maxAngleLineRenderer.enabled = false;
        }
    }

    /// <summary>
    /// Returns true if my muzzle is inside my target
    /// </summary>
    bool TargetTooClose()
    {
        // Where would my muzzle be if pointed directly at the target?
        float barrelLength = BarrelLength();
        Vector2 muzzleProjection = ((Vector2)aimTarget.position - (Vector2)projectileSourcePivot.position).normalized * barrelLength;
        Vector2 projMark = (Vector2)projectileSourcePivot.position + muzzleProjection;
        Debug.DrawLine(projectileSourcePivot.position, projMark, Color.magenta);

        // is my source inside my target?
        return aimTarget.GetComponent<Collider2D>().OverlapPoint(projMark);
    }

    /// <summary>
    /// Calculate the minimal angle to hit the aimTarget
    /// </summary>
    float CalculateMinAngle(float determ)
    {
        return MinAngle((Vector2)aimTarget.transform.position, determ) / deg2rad;
    }

    /// <summary>
    /// Calculate the maximal angle to hit the aimTarget
    /// </summary>
    float CalculateMaxAngle(float determ)
    {
        return MaxAngle((Vector2)aimTarget.transform.position, determ) / deg2rad;
    }

    /// <summary>
    /// Calculates the projectile's path over time and render it using the LineRenderer.
    /// Returns true if the path was blocked
    /// </summary>
    bool RenderPath(LineRenderer lr, float theta, bool? minFlag = null)
    {
        bool pathBlocked = false;
        // Turn angle into vector2 direction
        Vector2 dir = new Vector2() { x = (Mathf.Cos(theta * deg2rad)), y = Mathf.Sin(theta * deg2rad) }.normalized;
        Vector2 vel = dir * initialSpeed * (flipped ? -1 : 1);

        int numVerts = (int)(maxTime / timeResolution);
        lr.SetVertexCount(numVerts);

        int index = 0;

        Vector2 currentPosition = projectileSource.position;

        // set position for linerenderer point from time 0 to maxTime
        for (float t = 0.0f; t < maxTime; t += timeResolution)
        {
            if (index >= numVerts) break;
            lr.SetPosition(index, currentPosition);

            RaycastHit2D hit;

            //if (Physics.Raycast(currentPosition, velocityVector, out hit, velocityVector.magnitude * timeResolution, layerMask))
            if (hit = Physics2D.Raycast(currentPosition, vel, vel.magnitude * timeResolution, layerMask))
            {
                lr.SetVertexCount(index + 2);
                lr.SetPosition(index + 1, hit.point);

                // If i'm supposed to be aiming at a target, check if my path is obscured
                if (testMinAndMax && aimTarget != null)
                {
                    // Hit something, but it isn't my aim target!
                    if (!(hit.transform == aimTarget) && minFlag.HasValue)
                    {
                        if (!CloseEnoughToTarget(hit.point))
                        {
                            StartCoroutine(ResetAngleTimeout(minFlag.Value));
                            SetLineColor(lr, Color.red, Color.red);
                            pathBlocked = true;
                        }
                    }
                    // Hit my aim target!
                    else
                    {
                        SetLineColor(lr, Color.green, Color.green);
                    }
                }

                // stop the linerenderer here at the hit
                break;
            }

            // Normal line, or target out-of-range (if checking target)
            SetLineColor(lr, Color.blue, Color.blue);

            currentPosition += vel * timeResolution;
            vel += Physics2D.gravity * timeResolution;
            index++;
        }

        return pathBlocked;
    }

    /// <summary>
    /// Returns whether or not the given position is close enough to my target.
    /// Used to prevent a shot being considered blocked when it's actually on the target
    /// </summary>
    /// <returns></returns>
    bool CloseEnoughToTarget(Vector2 pos)
    {
        float buffer = 1f;
        float dist = ((Vector2)aimTarget.position - pos).magnitude;

        return dist <= buffer;
    }

    /// <summary>
    /// Determine the angle required to hit a target
    /// </summary>
    /// <source>https://en.wikipedia.org/wiki/Trajectory_of_a_projectile#Angle_required_to_hit_coordinate_.28x.2Cy.29</source>
    float CalculateAngle(Vector2 target, bool minAngle, float precalculatedDeterminant = -1)
    {
        Vector2 d = target - (Vector2)projectileSource.position;

        float denominator, determinant, f1, f2, result, g, v;
        v = flipped ? -initialSpeed : initialSpeed;
        g = -Physics2D.gravity.y;

        // Use precalculated determinant if you have it already
        if (precalculatedDeterminant != -1)
        {
            determinant = precalculatedDeterminant;
        }
        else
        {
            determinant = Mathf.Sqrt(
                Mathf.Pow(v, 4) - g * (g * Mathf.Pow(d.x, 2) + 2 * d.y * Mathf.Pow(v, 2))
            );
        }

        denominator = g * d.x;

        // do the damn calculations
        f1 = Mathf.Atan((Mathf.Pow(v, 2) + determinant) / denominator);
        f2 = Mathf.Atan((Mathf.Pow(v, 2) - determinant) / denominator);

        // Get the minimal angle
        if (minAngle)
        {
            result = flipped ? Mathf.Max(f1, f2) : Mathf.Min(f1, f2);
        }
        // Get the maximal angle
        else
        {
            result = flipped ? Mathf.Min(f1, f2) : Mathf.Max(f1, f2);
        }

        return result;
    }

    /// <summary>
    /// Returns the minimal angle to hit the target
    /// </summary>">
    float MinAngle(Vector2 target, float precalculatedDeterminant = -1)
    {
        return CalculateAngle(target, true, precalculatedDeterminant);
    }

    /// <summary>
    /// Returns the maximal angle to hit the target
    /// </summary>
    float MaxAngle(Vector2 target, float precalculatedDeterminant = -1)
    {
        return CalculateAngle(target, false, precalculatedDeterminant);
    }

    /// <summary>
    /// Calculate the determinant portion of the angle formula. Returns -1 if out of range.
    /// </summary>
    float TargetRangeCheck(Vector2 target)
    {
        Vector2 d = target - (Vector2)projectileSource.position;

        float determinant, g, v;
        v = flipped ? -initialSpeed : initialSpeed;
        g = -Physics2D.gravity.y;

        determinant = Mathf.Sqrt(
            Mathf.Pow(v, 4) - g * (g * Mathf.Pow(d.x, 2) + 2 * d.y * Mathf.Pow(v, 2))
            );

        return float.IsNaN(determinant) ? -1 : determinant;
    }

    /// <summary>
    /// Sets the flipped bool to true if the target is to the left
    /// </summary>
    void UpdateFlip(Vector2 target)
    {
        bool oldFlipped = flipped;

        Vector2 d = target - (Vector2)projectileSource.position;
        this.flipped = d.x < 0;

        if (oldFlipped != flipped)
        {
            angle += flipped ? 180 : -180;
            if (controlBarrel) UpdatePivot();
        }
    }

    /// <summary>
    /// After secs seconds, resets the minAngleObscured flag. This let's us try to 
    /// optimize angle if something moves out of the way after toggling it
    /// </summary>
    /// <param name="secs"></param>
    /// <returns></returns>
    IEnumerator ResetAngleTimeout(bool minFlag)
    {
        if (minFlag)
        {
            minBlockLock = true;
        }
        else
        {
            maxBlockLock = true;
        }

        yield return new WaitForSeconds(angleCheckCD);

        if (minFlag)
        {
            minAngleBlocked = false;
            minBlockLock = false;
        }
        else
        {
            maxAngleBlocked = false;
            maxBlockLock = false;
        }
    }

    /// <summary>
    /// Set the color of the LineRenderer
    /// </summary>
    void SetLineColor(LineRenderer lr, Color32 startCol, Color32 endCol)
    {
        lr.SetColors(startCol, endCol);
    }

    /// <summary>
    /// Updates the Z-rotation of the pivot point to match our angle
    /// </summary>
    void UpdatePivot()
    {
        projectileSourcePivot.rotation = Quaternion.AngleAxis(flipped ? angle + 180 : angle, new Vector3(0, 0, 1));
    }

    /// <summary>
    /// The length from the pivot to the muzzle
    /// </summary>
    float BarrelLength()
    {
        return (projectileSource.position - projectileSourcePivot.position).magnitude;
    }

    /// <summary>
    /// Lerps the angle of the barrel to an angle (theta) in the given time (animTime)
    /// </summary>
    public IEnumerator AimBarrel(float theta, float animTime)
    {
        float sourceTheta = angle;
        bool interrupted = false;

        // TODO this bool doesn't quite act like it should. If a lerp is interrupted, it goes to the new angle with the remaining lerp time
        // as opposed to starting over
        lerpingAngle = true;

        for (float elapsedTime = 0; elapsedTime < animTime; elapsedTime += Time.fixedDeltaTime)
        {
            if (!lerpingAngle)
            {
                interrupted = true;
                yield break;
            }
            angle = Mathf.LerpAngle(sourceTheta, theta, elapsedTime / animTime);

            yield return null;
        }
        if (!interrupted)
        {
            lerpingAngle = false;
        }
    }

    /// <summary>
    /// Determines the optimal angle to hit a target. If min angle is blocked, use max angle.
    /// If both min and max angles are blocked, then NaN is returned.
    /// </summary>
    float FindOptimalAngleTo(Vector2 target)
    {
        UpdateWithTarget(target);

        if (!minAngleBlocked)
            return minAngle;
        else if (!maxAngleBlocked)
            return maxAngle;
        else
            return float.NaN;
    }

    #region Tests
    [ContextMenu("LERP TO RANDOM ANGLE IN A SECOND")]
    void RandomLerp()
    {
        float theta = Random.Range(0, 360);
        StartCoroutine(AimBarrel(theta, 5));
    }

    [ContextMenu("LERP TO TARGET")]
    void UntargetedTargetLerp()
    {
        testMinAndMax = true;
        float theta = FindOptimalAngleTo(aimTarget.position);
        if (float.IsNaN(theta)) return;

        StartCoroutine(AimBarrel(theta, .5f));
    }
    #endregion
}
